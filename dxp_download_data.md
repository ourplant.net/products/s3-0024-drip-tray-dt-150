Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |[de](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150/-/raw/main/01_operating_manual/S3-0024_C_BA_Drip%20Tray.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150/-/raw/main/01_operating_manual/S3-0024_C_OM_Drip%20Tray.pdf)                  |
| assembly drawing         |  [de](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150/-/raw/main/02_assembly_drawing/s3-0024_A_ZNB_drip_tray_dt-150.PDF)                |
| circuit diagram          |                  |
| maintenance instructions | [de](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150/-/raw/main/04_maintenance_instructions/S3-0024_A_WA.Drip_Tray.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150/-/raw/main/04_maintenance_instructions/S3-0024_A_MI_Drip_Tray.pdf)                  |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150/-/raw/main/05_spare_parts/S3-0024_B_EVL%20Drip%20Tay.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0024-drip-tray-dt-150/-/raw/main/05_spare_parts/S3-0024_B_SWP_Drip_Tray.pdf)                   |

